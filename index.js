var net = require("net"),
  carrier = require("carrier");

var cont = 0;

var server = net.createServer(function (conn) {
  carrier.carry(conn, function (line) {
    console.log(`${cont}: ` + line);
    cont++;
  });
});
server.listen(4001);
